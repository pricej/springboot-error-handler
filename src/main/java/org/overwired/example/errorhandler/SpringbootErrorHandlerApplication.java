package org.overwired.example.errorhandler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootErrorHandlerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootErrorHandlerApplication.class, args);
	}
}
