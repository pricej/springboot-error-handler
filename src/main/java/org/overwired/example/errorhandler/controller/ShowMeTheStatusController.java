package org.overwired.example.errorhandler.controller;

import org.overwired.example.errorhandler.service.StatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Shows the results of calls to HttpStatusGenResponse.
 */
@RestController
public class ShowMeTheStatusController {
    private final StatusService statusService;

    @Autowired
    public ShowMeTheStatusController(StatusService statusService) {
        this.statusService = statusService;
    }


}
