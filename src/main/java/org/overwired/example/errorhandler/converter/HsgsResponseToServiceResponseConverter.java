package org.overwired.example.errorhandler.converter;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.overwired.example.errorhandler.domain.application.ServiceResponse;
import org.overwired.example.errorhandler.domain.service.HsgsResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Converts an HsgsResponse into a ServiceResponse.
 */
@Component
public class HsgsResponseToServiceResponseConverter implements Converter<HsgsResponse, ServiceResponse> {
    @Override
    public ServiceResponse convert(HsgsResponse source) {
        return new ServiceResponse(source.getTraceId(), source.getSpanId(), immutableCopyOf(source.getHeaders()));
    }

    private Map<String, ImmutableList<String>> immutableCopyOf(HttpHeaders httpHeaders) {
        Map<String, ImmutableList<String>> map = new LinkedHashMap<>();
        httpHeaders.entrySet().forEach(entry -> map.put(entry.getKey(), ImmutableList.copyOf(entry.getValue())));
        return ImmutableMap.copyOf(map);
    }
}
