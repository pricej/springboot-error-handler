package org.overwired.example.errorhandler.domain.application;

/**
 * An ErrorHandler application response.
 */
public class ErrorHandlerResponse {
    private final String traceId;
    private final String spanId;
    private final ServiceResponse serviceResponse;

    public ErrorHandlerResponse(String traceId, String spanId, ServiceResponse serviceResponse) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.serviceResponse = serviceResponse;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public ServiceResponse getServiceResponse() {
        return serviceResponse;
    }

}
