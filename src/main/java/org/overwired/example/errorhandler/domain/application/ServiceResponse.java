package org.overwired.example.errorhandler.domain.application;

import com.google.common.collect.ImmutableList;

import java.util.Map;

/**
 * The result of a service call.
 */
public class ServiceResponse {
    private final String traceId;
    private final String spanId;
    private final Map<String, ImmutableList<String>> headers;

    public ServiceResponse(String traceId, String spanId, Map<String, ImmutableList<String>> headers) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.headers = headers;
    }

    public String getTraceId() {
        return traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public Map<String, ImmutableList<String>> getHeaders() {
        return headers;
    }

}
