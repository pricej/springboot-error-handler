package org.overwired.example.errorhandler.domain.service;

import org.springframework.http.HttpHeaders;

/**
 * Represents the response we'll expect from the service we use.
 */
public class HsgsResponse {
    private String traceId;
    private String spanId;
    private HttpHeaders headers;

    public HsgsResponse() {
        // no-arg constructor since we have an all-args constructor
    }

    public HsgsResponse(String traceId, String spanId, HttpHeaders headers) {
        this.traceId = traceId;
        this.spanId = spanId;
        this.headers = headers;
    }

    public String getTraceId() {
        return traceId;
    }

    public void setTraceId(String traceId) {
        this.traceId = traceId;
    }

    public String getSpanId() {
        return spanId;
    }

    public void setSpanId(String spanId) {
        this.spanId = spanId;
    }

    public HttpHeaders getHeaders() {
        return headers;
    }

    public void setHeaders(HttpHeaders headers) {
        this.headers = headers;
    }

}
